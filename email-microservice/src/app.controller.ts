import { Controller, Get } from '@nestjs/common';
import { EventPattern } from '@nestjs/microservices';
const nodemailer = require('nodemailer');

@Controller()
export class AppController {
  constructor() { }
  
  @EventPattern('email_sending')
  async handleEmailSending(emailToSend) {
    try {
      const transport = nodemailer.createTransport({
        service: "Hotmail",
        auth: {
          user: emailToSend.user,
          pass: emailToSend.pass
        }
      });
      const mailOptions = {
        from: emailToSend.user,
        to: emailToSend.to,
        subject: emailToSend.subject,
        text: emailToSend.text
      };
  
      transport.sendMail(mailOptions, (error, info) => {
        if(error) {
          return console.log('Error while sending mail: ' + error);
        }
        else {
          console.log('Message sent');
        }
      });
  
      transport.close();
    }
    catch (error) {
      console.log(error);
    }  
  }
}
