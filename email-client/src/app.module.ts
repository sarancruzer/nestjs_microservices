import { Module } from '@nestjs/common';
import { Transport, ClientsModule } from '@nestjs/microservices';
import { AppController } from './app.controller';


@Module({
  imports: [
    ClientsModule.register([
      { name: 'EMAIL_SERVICE', transport: Transport.TCP },
    ]),
  ],
  controllers: [AppController],
  providers: [],
})
export class AppModule {}
