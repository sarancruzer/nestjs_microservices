import { Controller, Get, Inject } from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';
import { Email } from './email.event';

@Controller()
export class AppController {
  constructor(
    @Inject('EMAIL_SERVICE')
    private readonly client: ClientProxy) { }
  
  async onApplicationBootstrap() {
    await this.client.connect();
  }

  @Get()
  getSendEmail() {
    this.client.emit<any>('email_sending', new Email({
      user: '<FROM EMAIL>',
      pass: '<PASSWORD>',
      to: '<SEND TO WHO?>',
      subject: '<YOUR SUBJECT>',
      text: '<YOUR TEXT>'
    }));
    console.log('Email sent successfully');
    return 'Email sent';
  }

}
