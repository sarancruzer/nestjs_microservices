export class Email {
  user: string;
  pass: string;
  to: string;
  subject: string;
  text: string;

  constructor(emailToSend) {
    this.user = emailToSend.user;
    this.pass = emailToSend.pass;
    this.to = emailToSend.to;
    this.subject = emailToSend.subject;
    this.text = emailToSend.text;
  }
}